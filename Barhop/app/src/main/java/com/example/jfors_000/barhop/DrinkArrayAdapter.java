package com.example.jfors_000.barhop;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

//Created by JFORS_000 on 5/4/2015.

 public class DrinkArrayAdapter extends ArrayAdapter<SingleMenuItem> {

    private ArrayList<SingleMenuItem> data;

    public DrinkArrayAdapter(Context context, int resource,  ArrayList<SingleMenuItem> data) {
        super(context, resource, data);
        this.data = data;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        // check if view is null and inflate if it is
        if (convertView == null) {
            LayoutInflater layoutInflater = (LayoutInflater) parent.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.drink_list_item, null);
        }

        SingleMenuItem drinks = data.get(position);

        if (drinks != null) {

            TextView nameTextView = (TextView) convertView.findViewById(R.id.name_tv);
            TextView priceTextView = (TextView) convertView.findViewById(R.id.price_tv);
            TextView descriptionTextView = (TextView) convertView.findViewById(R.id.description_tv);

            nameTextView.setText(drinks.getName());
            double price = drinks.getPrice();
            priceTextView.setText(Double.toString(price));
            descriptionTextView.setText(drinks.getDescription());
        }
        // return converted view
        return convertView;
    }
}
