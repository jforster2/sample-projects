package com.example.jfors_000.barhop;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.util.Log;

import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

//Created by JFORS_000 on 4/10/2015.

public class ObtainBarsData extends AsyncTask<String, Void, String> {

    private ArrayList<Bar> obtainedBars;
    private BarsListActivity activity;
    private ProgressDialog retrieveBarsProgress;

    public ObtainBarsData(BarsListActivity barsListActivity) {
        obtainedBars = new ArrayList<>();
        activity = barsListActivity;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        // show a progress dialog so user can see progress of background thread retrieving bars
        retrieveBarsProgress = new ProgressDialog(activity);
        retrieveBarsProgress.setMessage("Retrieving Bars...");
        retrieveBarsProgress.setCancelable(false);
        retrieveBarsProgress.show();
    }

    @Override
    protected String doInBackground(String... params) {
        // stuff
        try {
            String state = params[0];
            String city = params[1];

            URL url = new URL("http://barhop.x10host.com/barhopPHP/obtainBars.php");

            // Add the data
            List values = new ArrayList(2);
            values.add(new BasicNameValuePair("state", state));
            values.add(new BasicNameValuePair("city", city));

            // convert to url encoded form entity
            UrlEncodedFormEntity entity = new UrlEncodedFormEntity(values);

            HttpURLConnection barsHttpURLConnection = (HttpURLConnection) url.openConnection();
            barsHttpURLConnection.setUseCaches(false);
            barsHttpURLConnection.setDoOutput(true);
            barsHttpURLConnection.setDoInput(true);

            barsHttpURLConnection.setRequestMethod("POST");
            OutputStream post = barsHttpURLConnection.getOutputStream();
            entity.writeTo(post);
            post.flush();

            BufferedReader returnedString = new BufferedReader(
                    new InputStreamReader(barsHttpURLConnection.getInputStream()));
            //StringBuilder buffer = new StringBuilder();
            String results = returnedString.readLine();

            post.close();
            returnedString.close();

            Log.i("JSON", results);

            if (results != null) {
                try {
                    JSONArray bars = new JSONArray(results);

                    for (int i = 0; i < bars.length(); i++) {
                        // set current element of JSON array to JSON object variable
                        JSONObject bar = bars.getJSONObject(i);

                        obtainedBars.add(new Bar(
                                bar.getInt(Bar.BAR_ID_KEY),
                                bar.getString(Bar.NAME_KEY),
                                bar.getString(Bar.ADDRESS_KEY),
                                bar.getString(Bar.OPENS_KEY),
                                bar.getString(Bar.CLOSES_KEY),
                                bar.getDouble(Bar.RATING_KEY),
                                bar.getDouble(Bar.LATITUDE_KEY),
                                bar.getDouble(Bar.LONGITUDE_KEY))
                        );
                    }
                    Log.i("OBTAINED BARS", obtainedBars.get(0).getName());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            return results;
        } catch (MalformedURLException e) {
            e.printStackTrace();
            return null;
        } catch (IOException e) {
            e.printStackTrace();
            Log.i("IOException", "returning null");
            return null;
        }
    }

    @Override
    protected void onPostExecute(String results) {
        if (retrieveBarsProgress.isShowing()) {
            retrieveBarsProgress.dismiss();
        }

        if (results == null) {
            Log.i("POST", "its null :(");
        } else {
            Log.i("POST", ":)");
            activity.setBars(obtainedBars);
        }
    }
}