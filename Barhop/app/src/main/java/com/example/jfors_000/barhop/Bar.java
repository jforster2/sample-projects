package com.example.jfors_000.barhop;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

//Created by Joshua Forster on 4/5/2015.

 public class Bar {

    // KEYS used in custom toBundle function to identify variables
    public static final String BAR_ID_KEY = "bar_id";
    public static final String USERNAME_KEY = "username";
    public static final String NAME_KEY = "name";
    public static final String ADDRESS_KEY = "address";
    public static final String PHONE_NUMBER_KEY = "phone_number";
    public static final String EMAIL_KEY = "email";
    public static final String OPENS_KEY = "opens";
    public static final String CLOSES_KEY = "closes";
    public static final String RATING_KEY = "rating";
    public static final String LATITUDE_KEY = "latitude";
    public static final String LONGITUDE_KEY = "longitude";
    public static final String CITY_KEY = "city";
    public static final String STATE_KEY = "state";
    public static final String PHOTO_URL_KEY = "photo_url";

    // member variables
    private int barID;
    private String username;
    private String name;
    private String address;
    private String phoneNumber;
    private String email;
    private String opens;
    private String closes;
    private double rating;
    private double latitude;
    private double longitude;
    private String city;
    private String state;
    private String photoURL;

    // for testing only
    /*public Bar(String name, double rating, String address) {
        this.name = name;
        this.rating = rating;
        this.address = address;
    }*/

    public Bar(int barID, String name, String address, String opens, String closes, double rating, double latitude, double longitude) {
        this.barID = barID;
        this.name = name;
        this.address = address;
        this.opens = opens;
        this.closes = closes;
        this.rating = rating;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public Bar(int barID, String username, String name, String address, String phoneNumber, String email, String opens, String closes, double rating, double latitude, double longitude, String city, String state, String photoURL) {
        this.barID = barID;
        this.username = username;
        this.name = name;
        this.address = address;
        this.phoneNumber = phoneNumber;
        this.email = email;
        this.opens = opens;
        this.closes = closes;
        this.rating = rating;
        this.latitude = latitude;
        this.longitude = longitude;
        this.city = city;
        this.state = state;
        this.photoURL = photoURL;
    }

    public int getBarID() {
        return barID;
    }

    public void setBarID(int barID) {
        this.barID = barID;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getOpens() {
        return opens;
    }

    public void setOpens(String opens) {
        this.opens = opens;
    }

    public String getCloses() {
        return closes;
    }

    public void setCloses(String closes) {
        this.closes = closes;
    }

    public double getRating() {
        return rating;
    }

    public void setRating(double rating) {
        this.rating = rating;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getPhotoURL() {
        return photoURL;
    }

    public void setPhotoURL(String photoURL) {
        this.photoURL = photoURL;
    }

    public Bundle toBundle(){

        Bundle bundle = new Bundle( );

        bundle.putInt(BAR_ID_KEY, barID);
        bundle.putString(USERNAME_KEY, username);
        bundle.putString(NAME_KEY, name);
        bundle.putString(ADDRESS_KEY, address);
        bundle.putString(PHONE_NUMBER_KEY, phoneNumber);
        bundle.putString(EMAIL_KEY, email);
        bundle.putString(OPENS_KEY, opens );
        bundle.putString(CLOSES_KEY, closes);
        bundle.putDouble(RATING_KEY, rating);
        bundle.putDouble(LATITUDE_KEY, latitude);
        bundle.putDouble(LONGITUDE_KEY, longitude);
        bundle.putString(CITY_KEY, city);
        bundle.putString(STATE_KEY, state);
        bundle.putString(PHOTO_URL_KEY, photoURL);

        return bundle;
    }

    public static Bar fromBundle(Bundle bundle) {
        return new Bar(
                bundle.getInt(BAR_ID_KEY),
                bundle.getString(USERNAME_KEY),
                bundle.getString(NAME_KEY),
                bundle.getString(ADDRESS_KEY),
                bundle.getString(PHONE_NUMBER_KEY),
                bundle.getString(EMAIL_KEY),
                bundle.getString(OPENS_KEY),
                bundle.getString(CLOSES_KEY),
                bundle.getDouble(RATING_KEY),
                bundle.getDouble(LATITUDE_KEY),
                bundle.getDouble(LONGITUDE_KEY),
                bundle.getString(CITY_KEY),
                bundle.getString(STATE_KEY),
                bundle.getString(PHOTO_URL_KEY)
        );
    }

    public void setContactInformationFragment(View view) {
        Log.i("INSIDE SET FRAGMENT", "BEFORE IF");
        if (view != null) {
            Log.i("INSIDE SET FRAGMENT", "INSIDE IF");
            TextView textView = (TextView) view.findViewById(R.id.bar_name_tv);
            textView.setText(this.getName());

            textView = (TextView) view.findViewById(R.id.address_tv);
            textView.setText(this.getAddress());

            textView = (TextView) view.findViewById(R.id.phone_number_tv);
            textView.setText(this.getPhoneNumber());

            textView = (TextView) view.findViewById(R.id.email_tv);
            textView.setText(this.getEmail());

            textView = (TextView) view.findViewById(R.id.opens_tv);
            textView.setText(this.getOpens());

            textView = (TextView) view.findViewById(R.id.closes_tv);
            textView.setText(this.getCloses());

            textView = (TextView) view.findViewById(R.id.city_tv);
            textView.setText(this.getCity());

            textView = (TextView) view.findViewById(R.id.state_tv);
            textView.setText(this.getState());
        }
    }
}