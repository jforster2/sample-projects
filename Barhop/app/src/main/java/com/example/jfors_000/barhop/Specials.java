package com.example.jfors_000.barhop;

import android.os.Bundle;
import android.view.View;
import android.widget.ListView;

import java.util.ArrayList;

//Created by Joshua Forster on 5/2/2015.

public class Specials {

    // KEYS for members
    public static final String TIME_KEY = "time";
    public static final String PRICE_KEY = "price";
    public static final String NAME_KEY = "name";

    private ArrayList<Special> specials;

    public Specials() {
        this.specials = new ArrayList<>();
    }

    protected void addSpecial(Special special) {
        this.specials.add(special);
    }

    public ArrayList<Special> getSpecials() {
        return specials;
    }

    public void setSpecials(ArrayList<Special> specials) {
        this.specials = specials;
    }

    public Special getSpecialAt(int index) {
        return this.specials.get(index);
    }

    public Bundle toBundle() {
        Bundle bundle = new Bundle();
        int i = 0;

        for (Special special : specials) {
            bundle.putString(TIME_KEY + i, special.getTime());
            bundle.putString(PRICE_KEY + i, special.getPrice());
            bundle.putString(NAME_KEY + i, special.getName());
            i++;
        }

        return bundle;
    }

    public static Specials fromBundle (Bundle bundle) {
        Specials specials = new Specials();

        for (int i = 0; i < (bundle.size()/3); i++) {
            specials.addSpecial(new Special(
                    bundle.getString(TIME_KEY + i),
                    bundle.getString(PRICE_KEY + i),
                    bundle.getString(NAME_KEY + i)
            ));
        }
        return specials;
    }

    public void setSpecialsList(ListView listView, View view) {
        SpecialArrayAdapter specialArrayAdapter = new SpecialArrayAdapter(view.getContext(), R.layout.special_list_item, this.specials);
        listView.setAdapter(specialArrayAdapter);
    }
}
