package com.example.jfors_000.barhop;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;


public class LoginActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        // Set a toolbar to replace the action bar.
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if (!isNetworkAvailable()) {
            TextView errorMessage = (TextView) findViewById(R.id.error_message);
            errorMessage.setText("NO INTERNET CONNECTION! PLEASE CHECK CONNECTION AND TRY AGAIN");
        } else {
            Button login = (Button) findViewById(R.id.login_button);
            Button createAccount = (Button) findViewById(R.id.create_account_button);
            login.setEnabled(true);
            createAccount.setEnabled(true);
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_login, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    // checks if user is connected to internet when resuming login
    @Override
    protected void onResume() {
        super.onResume();

        TextView errorMessage = (TextView) findViewById(R.id.error_message);

        if (!isNetworkAvailable()) {
            errorMessage.setText("NO INTERNET CONNECTION! PLEASE CHECK CONNECTION AND TRY AGAIN");
        } else {
            Button login = (Button) findViewById(R.id.login_button);
            Button createAccount = (Button) findViewById(R.id.create_account_button);
            login.setEnabled(true);
            createAccount.setEnabled(true);
            errorMessage.setText("");
        }
    }

    // keep user from going back to start page
    @Override
    public void onBackPressed() {
        // Don't go back ;)
    }

    public void confirmLogin(View view) {
        EditText username = (EditText) findViewById(R.id.username_et);
        EditText password = (EditText) findViewById(R.id.password_et);
        ConfirmLogin request = new ConfirmLogin(LoginActivity.this);
        request.execute(username.getText().toString(), password.getText().toString());
    }

    /** takes user to another page to create their account if a new user */
    public void createNewAccount(View view) {
        Intent intent = new Intent(getApplicationContext(), CreateAccountActivity.class);
        startActivity(intent);
    }

    /** login to user account and take them to home page */
    public void loginConfirmed() {
        Intent intent = new Intent(getApplicationContext(), BarsListActivity.class);
        startActivity(intent);
    }

    public void loginFailed(String message) {
        TextView failedLoginMessage = (TextView) findViewById(R.id.error_message);
        failedLoginMessage.setText(message);
    }

    // Check if connected to network
    public boolean isNetworkAvailable() {
        ConnectivityManager connection = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connection.getActiveNetworkInfo();

        if (networkInfo != null && networkInfo.isConnected()) {
            return true;    // connection established :)
        }

        return false;   // no connection :(
    }
}
