package com.example.jfors_000.barhop;

//Created by Joshua Forster on 5/4/2015.

 public class SingleMenuItem {

    public static final String DESCRIPTION_KEY = "description";
    public static final String PRICE_KEY = "price";
    public static final String NAME_KEY = "name";
    public static final String CATEGORY_ID_KEY = "category_id";

    private String description;
    private int category_id;
    private double price;
    private String name;

    public SingleMenuItem(String description, int category_id, double price, String name) {
        this.description = description;
        this.category_id = category_id;
        this.price = price;
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getCategory_id() {
        return category_id;
    }

    public void setCategory_id(int category_id) {
        this.category_id = category_id;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
