package com.example.jfors_000.barhop;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;


public class CreateAccountActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_account);

        // Set a toolbar to replace the action bar.
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_create_account, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void confirmAccountCreation(View view) {
        EditText username = (EditText) findViewById(R.id.username_et);
        EditText password = (EditText) findViewById(R.id.password_et);
        EditText email = (EditText) findViewById(R.id.email_et);
        EditText first_name = (EditText) findViewById(R.id.first_name_et);
        EditText last_name = (EditText) findViewById(R.id.last_name_et);
        EditText confirm_password = (EditText) findViewById(R.id.password_confirm_et);
        EditText confirm_email = (EditText) findViewById(R.id.email_confirm_et);


         if (!(password.getText().toString().equals(confirm_password.getText().toString()) )){
            TextView failedCreateAccountMessage = (TextView) findViewById(R.id.error_message);
            failedCreateAccountMessage.setText("Make sure password and confirm password are the same");
        }
        else if (!(email.getText().toString().equals(confirm_email.getText().toString()) )){
            TextView failedCreateAccountMessage = (TextView) findViewById(R.id.error_message);
            failedCreateAccountMessage.setText("Make sure email and confirm email are the same");
        }
         else if ( (password.getText().toString().equals(confirm_password.getText().toString()) ) && (email.getText().toString().equals(confirm_email.getText().toString()) )) {
            ConfirmCreateAccount request = new ConfirmCreateAccount(CreateAccountActivity.this);
            request.execute(username.getText().toString(), password.getText().toString(), email.getText().toString(), first_name.getText().toString(), last_name.getText().toString());
        }
    }

    public void createAccount() {
        Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
        startActivity(intent);
    }
    public void createAccountFailed(String message) {
        TextView failedCreateAccountMessage = (TextView) findViewById(R.id.error_message);
        failedCreateAccountMessage.setText(message);
    }
}
