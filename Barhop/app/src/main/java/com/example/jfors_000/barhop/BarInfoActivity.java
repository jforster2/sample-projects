package com.example.jfors_000.barhop;

import android.content.Intent;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;


public class BarInfoActivity extends ActionBarActivity implements ContactInformationFragment.OnDirectionsRequestedListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bar_info);
        Log.i("STEP 1", "ONCREATE BARINFO");

        // Set a toolbar to replace the action bar.
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle(getIntent().getStringExtra(Bar.NAME_KEY));
        setSupportActionBar(toolbar);

        //retrieveBarDataProgress = new ProgressDialog(this);

        Log.i("STEP 2", "RETRIEVE DATA");
        ObtainSelectedBarData obtainSelectedBarData = new ObtainSelectedBarData(this);
        obtainSelectedBarData.execute(getIntent().getExtras().getInt(Bar.BAR_ID_KEY));

        /*TabPagerAdapter tabAdapter = new TabPagerAdapter(getFragmentManager(), bundle);
        ViewPager displayedTab = (ViewPager) findViewById(R.id.tabPager);
        displayedTab.setAdapter(tabAdapter);*/
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_bar_info, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onDirectionsRequested(View view, double latitude, double longitude) {
        //Log.i("ACTIVITY", "INSIDE LISTENER FUNCTION");
        if (view.getId() == R.id.mapButton) {
            //Log.i("ACTIVITY", "INSIDE BUTTON PRESS");
            Intent intent = new Intent(getApplicationContext(), MappingActivity.class);
            intent.putExtra(Bar.LATITUDE_KEY, latitude);
            intent.putExtra(Bar.LONGITUDE_KEY, longitude);
            startActivity(intent);
        }
    }

    public void populateTabs(Bar bar, Specials specials, MenuItems menuItems) {
        Log.i("STEP 4", "POPULATE TABS");
        TabPagerAdapter tabAdapter = new TabPagerAdapter(getFragmentManager(), bar, specials, menuItems);
        ViewPager displayedTab = (ViewPager) findViewById(R.id.tabPager);
        displayedTab.setAdapter(tabAdapter);
    }
}
