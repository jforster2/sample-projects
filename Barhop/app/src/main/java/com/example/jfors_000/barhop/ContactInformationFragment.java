package com.example.jfors_000.barhop;

import android.app.Activity;
import android.os.Bundle;
import android.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link com.example.jfors_000.barhop.ContactInformationFragment.OnDirectionsRequestedListener} interface
 * to handle interaction events.
 * Use the {@link ContactInformationFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ContactInformationFragment extends Fragment implements OnClickListener {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_BAR_ID = "bar_id";

    // TODO: Rename and change types of parameters
    private int id;
    private Bar bar;
    private View view;


    private OnDirectionsRequestedListener directionsListener;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * //@param param1 Parameter 1.
     * @return A new instance of fragment ContactInformationFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ContactInformationFragment newInstance(Bar bar) {
        ContactInformationFragment fragment = new ContactInformationFragment();
        if (bar != null) {
            Bundle args = new Bundle();
            args.putAll(bar.toBundle());
            fragment.setArguments(args);
        }
        return fragment;
    }

    public ContactInformationFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            // add from bundle to bar class
            id = getArguments().getInt(ARG_BAR_ID);
            bar = Bar.fromBundle(getArguments());
            Log.i("STEP 6", "SHOW DATA");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        Log.i("STEP 7", "INFLATE VIEW");
        view = inflater.inflate(R.layout.fragment_contact_information, container, false);
        view.findViewById(R.id.mapButton).setOnClickListener(this);
        bar.setContactInformationFragment(view);
        return view;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            directionsListener = (OnDirectionsRequestedListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnDirectionsRequestedListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        directionsListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnDirectionsRequestedListener {
        void onDirectionsRequested(View view, double latitude, double longitude);
    }

    @Override
    public void onClick(View view) {
        Log.i("ONCLICK", "HERE");
        if (directionsListener != null) {
            directionsListener.onDirectionsRequested(view, bar.getLatitude(), bar.getLongitude());
        }
    }
}