package com.example.jfors_000.barhop;

import android.os.AsyncTask;
import android.util.Log;

import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.message.BasicNameValuePair;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

//Created by Jon Moss on 4/30/2015.

public class ConfirmCreateAccount extends AsyncTask<String, Void, String> {

    URL url;
    HttpURLConnection createAccountHttpURLConnection;
    String validation;
    CreateAccountActivity activity;

    public  ConfirmCreateAccount(CreateAccountActivity createAccountActivity) { activity = createAccountActivity; }


    @Override
    protected String doInBackground(String... params) {
        // stuff
        try {
            String username = params[0];
            String password = params[1];
            String email = params[2];
            String first_name = params[3];
            String last_name = params[4];

            url = new URL("http://barhop.x10host.com/barhopPHP/mobileCreateAccount.php");

            // Add the data
            List values = new ArrayList(5);
            values.add(new BasicNameValuePair("username", username));
            values.add(new BasicNameValuePair("password", password));
            values.add(new BasicNameValuePair("email", email));
            values.add(new BasicNameValuePair("first_name", first_name));
            values.add(new BasicNameValuePair("last_name", last_name));

            // convert to url encoded form entity
            UrlEncodedFormEntity entity = new UrlEncodedFormEntity(values);

            createAccountHttpURLConnection = (HttpURLConnection) url.openConnection();
            createAccountHttpURLConnection.setUseCaches(false);
            createAccountHttpURLConnection.setDoOutput(true);
            createAccountHttpURLConnection.setDoInput(true);

            createAccountHttpURLConnection.setRequestMethod("POST");
            OutputStream post = createAccountHttpURLConnection.getOutputStream();
            entity.writeTo(post);
            post.flush();

            BufferedReader returnedString = new BufferedReader(
                    new InputStreamReader(createAccountHttpURLConnection.getInputStream()));
            validation = returnedString.readLine();

            returnedString.close();
            post.close();
            Log.i("RESULTS", validation);

            return validation;
        } catch (MalformedURLException e) {
            e.printStackTrace();
            return null;
        } catch (IOException e) {
            e.printStackTrace();
            Log.i("IOException", "returning null");
            return null;
        }
    }
    @Override
    protected void onPostExecute(String results) {
        if (results == null) {
            Log.i("POST", "its null :(");
        } else {
            Log.i("POST", "--" + results + "--");
            if ("SUCCESS".equals(results)) {
                activity.createAccount();
            } else {
                activity.createAccountFailed(results);
            }
        }
    }
}
