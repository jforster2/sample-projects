package com.example.jfors_000.barhop;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.util.Log;

import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

//Created by Joshua Forster on 4/28/2015.

 public class ObtainSelectedBarData extends AsyncTask<Integer, Void, Boolean> {

    private Bar obtainedBar;
    private Specials obtainedSpecials = new Specials();
    private MenuItems obtainedMenuItems = new MenuItems();
    private BarInfoActivity activity;
    private ProgressDialog retrieveBarDataProgress;

    public ObtainSelectedBarData(BarInfoActivity activity) {
        Log.i("STEP 3", "THREAD CONSTRUCTOR");
        this.activity = activity;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        // show a progress dialog so user can see progress of background thread retrieving bar data
        retrieveBarDataProgress = new ProgressDialog(activity);
        retrieveBarDataProgress.setMessage("Retrieving Bar's Data...");
        retrieveBarDataProgress.setCancelable(false);
        retrieveBarDataProgress.show();
    }

    @Override
    protected Boolean doInBackground(Integer... params) {

        try {
            int selectedBarId = params[0];

            //Log.i("DO IN BACK", "ID = " + selectedBarId);

            URL url = new URL("http://barhop.x10host.com/barhopPHP/obtainBarInformation.php");

            // Add data to POST
            List values = new ArrayList(1);
            values.add(new BasicNameValuePair(Bar.BAR_ID_KEY, Integer.toString(selectedBarId)));

            // Convert to url encoded form entity
            UrlEncodedFormEntity entity = new UrlEncodedFormEntity(values);

            HttpURLConnection barHttpURLConnection = (HttpURLConnection) url.openConnection();
            barHttpURLConnection.setUseCaches(false);
            barHttpURLConnection.setDoOutput(true);
            barHttpURLConnection.setDoInput(true);

            barHttpURLConnection.setRequestMethod("POST");
            OutputStream post = barHttpURLConnection.getOutputStream();
            entity.writeTo(post);
            post.flush();

            BufferedReader returnedString = new BufferedReader(
                    new InputStreamReader(barHttpURLConnection.getInputStream()));
            String jsonResults = returnedString.readLine();

            post.close();
            returnedString.close();

            Log.i("JSON", jsonResults);

            if (jsonResults != null) {
                try {
                    Log.i("INSIDE TRY", "OBTAINING DATA");
                    JSONArray barData = new JSONArray(jsonResults);

                    //JSONObject bar = new JSONObject(jsonResults);
                    JSONObject bar = barData.getJSONObject(0);
                    String specialData = barData.getString(1);
                    String menuData = barData.getString(2);
                    JSONArray specials = new JSONArray(specialData);
                    JSONArray menuItems = new JSONArray(menuData);

                    obtainedBar = new Bar(
                            bar.getInt(Bar.BAR_ID_KEY),
                            bar.getString(Bar.USERNAME_KEY),
                            bar.getString(Bar.NAME_KEY),
                            bar.getString(Bar.ADDRESS_KEY),
                            bar.getString(Bar.PHONE_NUMBER_KEY),
                            bar.getString(Bar.EMAIL_KEY),
                            bar.getString(Bar.OPENS_KEY),
                            bar.getString(Bar.CLOSES_KEY),
                            bar.getDouble(Bar.RATING_KEY),
                            bar.getDouble(Bar.LATITUDE_KEY),
                            bar.getDouble(Bar.LONGITUDE_KEY),
                            bar.getString(Bar.CITY_KEY),
                            bar.getString(Bar.STATE_KEY),
                            bar.getString(Bar.PHOTO_URL_KEY)
                    );

                    Log.i("AFTER BAR", "BEFORE SPECIALS FOR LOOP");
                    Log.i("LENGTH", "length = " + specials.length());
                    for (int i = 0; i < specials.length(); i++) {
                        Log.i("AFTER BAR", "INSIDE SPECIALS FOR LOOP");
                        // set current element of JSON array to JSON object
                        JSONObject special = specials.getJSONObject(i);

                        obtainedSpecials.addSpecial(new Special(
                                        special.getString(Special.TIME_KEY),
                                        special.getString(Special.PRICE_KEY),
                                        special.getString(Special.NAME_KEY))
                        );
                    }

                    for (int i = 0; i < menuItems.length(); i++) {
                        Log.i("AFTER BAR", "INSIDE SPECIALS FOR LOOP");
                        // set current element of JSON array to JSON object
                        JSONObject menuItem = menuItems.getJSONObject(i);

                        obtainedMenuItems.addSingleMenuItem(new SingleMenuItem(
                                        menuItem.getString(MenuItems.DESCRIPTION_KEY),
                                        menuItem.getInt(MenuItems.CATEGORY_ID_KEY),
                                        menuItem.getDouble(MenuItems.PRICE_KEY),
                                        menuItem.getString(MenuItems.NAME_KEY))
                        );
                    }
                    return true;
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            return false;
        } catch (MalformedURLException e) {
            e.printStackTrace();
            return false;
        } catch (IOException e) {
            e.printStackTrace();
            Log.i("IOException", "returning null");
            return false;
        }
    }

    @Override
    protected void onPostExecute(Boolean success) {
        if (retrieveBarDataProgress.isShowing()) {
            retrieveBarDataProgress.dismiss();
        }

        if (success) {
            Log.i("POST", ":)");
            activity.populateTabs(obtainedBar, obtainedSpecials, obtainedMenuItems);
        } else {
            Log.i("POST", "its null :(");
        }
    }
}