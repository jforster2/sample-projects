package com.example.jfors_000.barhop;

import android.os.Bundle;
import android.view.View;
import android.widget.ListView;

import java.util.ArrayList;

//Created by Joshua Forster on 5/4/2015.

public class MenuItems {

    public static final String DESCRIPTION_KEY = "description";
    public static final String PRICE_KEY = "price";
    public static final String NAME_KEY = "name";
    public static final String CATEGORY_ID_KEY = "category_id";

    private ArrayList<SingleMenuItem> drinks;
    private ArrayList<SingleMenuItem> food;

    public MenuItems() {
        this.drinks = new ArrayList<>();
        this.food = new ArrayList<>();
    }

    public void addSingleMenuItem(SingleMenuItem item) {
        if (item.getCategory_id() == 1) {
            this.drinks.add(item);
        } else {
            this.food.add(item);
        }
    }

    public ArrayList<SingleMenuItem> getDrinks() {
        return drinks;
    }

    public void setDrinks(ArrayList<SingleMenuItem> drinks) {
        this.drinks = drinks;
    }

    public ArrayList<SingleMenuItem> getFood() {
        return food;
    }

    public void setFood(ArrayList<SingleMenuItem> food) {
        this.food = food;
    }

    public Bundle toBundleDrinks(ArrayList<SingleMenuItem> drinks) {
        Bundle bundle = new Bundle();
        int i = 0;

        for (SingleMenuItem singleMenuItem: drinks) {
            bundle.putString(DESCRIPTION_KEY + i, singleMenuItem.getDescription());
            bundle.putDouble(PRICE_KEY + i, singleMenuItem.getPrice());
            bundle.putString(NAME_KEY + i, singleMenuItem.getName());
            bundle.putInt(CATEGORY_ID_KEY + i, singleMenuItem.getCategory_id());
            i++;
        }
        return bundle;
    }

    public Bundle toBundleFood(ArrayList<SingleMenuItem> food) {
        Bundle bundle = new Bundle();
        int i = 0;

        for (SingleMenuItem singleMenuItem: food) {
            bundle.putString(DESCRIPTION_KEY + i, singleMenuItem.getDescription());
            bundle.putDouble(PRICE_KEY + i, singleMenuItem.getPrice());
            bundle.putString(NAME_KEY + i, singleMenuItem.getName());
            bundle.putInt(CATEGORY_ID_KEY + i, singleMenuItem.getCategory_id());
            i++;
        }
        return bundle;
    }

    public static MenuItems fromBundleDrinks (Bundle bundle) {
        MenuItems menuItems = new MenuItems();

        for (int i = 0; i < (bundle.size() / 3); i++) {
            menuItems.addSingleMenuItem(new SingleMenuItem(
                    bundle.getString(DESCRIPTION_KEY + i),
                    bundle.getInt(CATEGORY_ID_KEY + i),
                    bundle.getDouble(PRICE_KEY + i),
                    bundle.getString(NAME_KEY + i)
            ));
        }
        return menuItems;
    }

    public static MenuItems fromBundleFood (Bundle bundle) {
        MenuItems menuItems = new MenuItems();

        for (int i = 0; i < (bundle.size() / 3); i++) {
            menuItems.addSingleMenuItem(new SingleMenuItem(
                    bundle.getString(DESCRIPTION_KEY + i),
                    bundle.getInt(CATEGORY_ID_KEY + i),
                    bundle.getDouble(PRICE_KEY + i),
                    bundle.getString(NAME_KEY + i)
            ));
        }
        return menuItems;
    }

    public void setDrinksList(ListView listView, View view) {
        DrinkArrayAdapter drinksArrayAdapter = new DrinkArrayAdapter(view.getContext(), R.layout.drink_list_item, this.drinks);
        listView.setAdapter(drinksArrayAdapter);
    }

    public void setFoodList(ListView listView, View view) {
        FoodArrayAdapter foodArrayAdapter = new FoodArrayAdapter(view.getContext(), R.layout.food_list_item, this.food);
        listView.setAdapter(foodArrayAdapter);
    }
}
