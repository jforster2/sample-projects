package com.example.jfors_000.barhop;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

//Created by Joshua Forster on 4/30/2015.

 public class SpecialArrayAdapter extends ArrayAdapter<Special> {

    private ArrayList<Special> data;

    public SpecialArrayAdapter(Context context, int resource,  ArrayList<Special> data) {
        super(context, resource, data);
        this.data = data;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        // check if view is null and inflate if it is
        if (convertView == null) {
            LayoutInflater layoutInflater = (LayoutInflater) parent.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.special_list_item, null);
        }

        Special special = data.get(position);

        if (special != null) {

            TextView timeTextView = (TextView) convertView.findViewById(R.id.time_tv);
            TextView priceTextView = (TextView) convertView.findViewById(R.id.price_tv);
            TextView descriptionTextView = (TextView) convertView.findViewById(R.id.name_tv);

            timeTextView.setText(special.getTime());
            priceTextView.setText(special.getPrice());
            descriptionTextView.setText(special.getName());
        }
        // return converted view
        return convertView;
    }
}