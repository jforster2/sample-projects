package com.example.jfors_000.barhop;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;

public class MappingActivity extends ActionBarActivity {

    private static final String LATITUDE_KEY = "latitude";
    private static final String LONGITUDE_KEY = "longitude";

    private GoogleMap directionsMap;
    private double latitude;
    private double longitude;
    private LatLng start, barLocation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mapping);

        // Set a toolbar to replace the action bar.
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //directionsMap.setMyLocationEnabled(true);
        start = new LatLng(36.5402586, -87.3438526);
        latitude = getIntent().getDoubleExtra(LATITUDE_KEY, start.latitude);
        longitude = getIntent().getDoubleExtra(LONGITUDE_KEY, start.longitude);
        /*barLocation = new LatLng(latitude, longitude);

        // Create map object and markers
        directionsMap = ((MapFragment) getFragmentManager().findFragmentById(R.id.map)).getMap();
        Marker current = directionsMap.addMarker(new MarkerOptions().title("Current Location").position(start));
        Marker barMarker = directionsMap.addMarker(new MarkerOptions().title("Current Location").position(barLocation));

        directionsMap.moveCamera(CameraUpdateFactory.newLatLngZoom(start, 15));*/

        //Get current Location
        //directionsMap.setMyLocationEnabled(true);

        Uri gmmIntentUri = Uri.parse("google.navigation:q=" + latitude + "," + longitude + "");
        Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
        mapIntent.setPackage("com.google.android.apps.maps");
        startActivity(mapIntent);

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_mapping, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
