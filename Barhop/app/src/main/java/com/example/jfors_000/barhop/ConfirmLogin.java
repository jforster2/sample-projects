package com.example.jfors_000.barhop;

import android.os.AsyncTask;
import android.util.Log;

import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.message.BasicNameValuePair;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

//Created by Joshua Forster on 4/25/2015.

 public class ConfirmLogin extends AsyncTask<String, Void, String> {

    URL url;
    HttpURLConnection loginHttpURLConnection;
    String validation;
    LoginActivity activity;

    public ConfirmLogin(LoginActivity loginActivity) {
        activity = loginActivity;
    }

    @Override
    protected String doInBackground(String... params) {
        // stuff
        try {
            String username = params[0];
            String password = params[1];

            url = new URL("http://barhop.x10host.com/barhopPHP/mobileLogin.php");

            // Add the data
            List values = new ArrayList(2);
            values.add(new BasicNameValuePair("username", username));
            values.add(new BasicNameValuePair("password", password));

            // convert to url encoded form entity
            UrlEncodedFormEntity entity = new UrlEncodedFormEntity(values);

            loginHttpURLConnection = (HttpURLConnection) url.openConnection();
            loginHttpURLConnection.setUseCaches(false);
            loginHttpURLConnection.setDoOutput(true);
            loginHttpURLConnection.setDoInput(true);

            loginHttpURLConnection.setRequestMethod("POST");
            OutputStream post = loginHttpURLConnection.getOutputStream();
            entity.writeTo(post);
            post.flush();

            BufferedReader returnedString = new BufferedReader(
                    new InputStreamReader(loginHttpURLConnection.getInputStream()));
            //StringBuilder buffer = new StringBuilder();
            validation = returnedString.readLine();

            returnedString.close();
            post.close();

            Log.i("RESULTS", validation);

            return validation;
        } catch (MalformedURLException e) {
            e.printStackTrace();
            return null;
        } catch (IOException e) {
            e.printStackTrace();
            Log.i("IOException", "returning null");
            return null;
        }
    }

    @Override
    protected void onPostExecute(String results) {
        if (results == null) {
            Log.i("POST", "its null :(");
        } else {
            Log.i("POST", "--" + results + "--");
            if ("valid".equals(results)) {
                activity.loginConfirmed();
            } else {
                activity.loginFailed(results);
            }
        }
    }
}
