package com.example.jfors_000.barhop;

import android.app.Fragment;
import android.app.FragmentManager;
import android.support.v13.app.FragmentPagerAdapter;
import android.util.Log;

//Created by Joshua Forster on 4/21/2015.

public class TabPagerAdapter extends FragmentPagerAdapter {

    static final int NUM_TABS = 4;
    private Bar barInformation;
    private Specials specials;
    private MenuItems items;
    //private Bundle data;

    public TabPagerAdapter(FragmentManager fm, Bar barData, Specials specials, MenuItems menuItems) {
        super(fm);
        this.barInformation = barData;
        this.specials = specials;
        this.items = menuItems;
    }

    @Override
    public int getCount() {
        return NUM_TABS;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                Log.i("STEP 5", "NEW INSTANCE OF TAB1");
                /*ContactInformationFragment tab1 = ContactInformationFragment.newInstance(barInformation);
                tab1.setArguments(barInformation.toBundle());
                tab1.setDirectionsListener(this);
                return tab1;   // tab 1*/
                return ContactInformationFragment.newInstance(barInformation);   // tab 1
            case 1:
                /*SpecialsFragment tab2 = SpecialsFragment.newInstance(specials);
                tab2.setArguments(data);
                return tab2;   // tab 2*/
                return SpecialsFragment.newInstance(specials);   // tab 2
            case 2:
                /*DrinksFragment tab3 = DrinksFragment.newInstance(items);
                tab2.setArguments(data);
                return tab3;   // tab 3*/
                return DrinksFragment.newInstance(items);   // tab 3
            case 3:
                /*FoodFragment tab4 = FoodFragment.newInstance(items);
                tab2.setArguments(data);
                return tab4;   // tab 4*/
                return FoodFragment.newInstance(items);   // tab 4
        }
        return null;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return "Contact Information";   // tab 1 title
            case 1:
                return "Specials";   // tab 2 title
            case 2:
                return "Drinks";   // tab 3 title
            case 3:
                return "Food";   // tab 44 title
        }
        return null;
    }
}