package com.example.jfors_000.barhop;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.RatingBar;
import android.widget.TextView;

import java.util.ArrayList;

//Created by Joshua Forster on 4/10/2015.

public class BarArrayAdapter extends ArrayAdapter<Bar> {

    private ArrayList<Bar> data;

    public BarArrayAdapter(Context context, int resource,  ArrayList<Bar> data) {
        super(context, resource, data);
        this.data = data;
    }

    public View getView(int position, View convertView, ViewGroup parent) {

        // check if view is null; If yes inflate it
        if (convertView == null) {
            LayoutInflater layoutInflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.bar_list_item, null);
        }

        /*
         * Position is passed in as argument to this method.
         * It is the current position of the item that was
         * clicked on by the user.
         */
        Bar bar = data.get(position);

        if (bar != null) {

            TextView nameTextView = (TextView) convertView.findViewById(R.id.bar_name_tv);
            TextView distanceTextView = (TextView) convertView.findViewById(R.id.address_tv);
            RatingBar ratingBar = (RatingBar) convertView.findViewById(R.id.rating_rb);

            nameTextView.setText(bar.getName());
            distanceTextView.setText(bar.getAddress());
            ratingBar.setMax(5);
            ratingBar.setRating( (float) bar.getRating() );
        }

        // return converted view
        return convertView;
    }
}
