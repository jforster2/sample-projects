package com.example.jfors_000.barhop;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

//Created by Joshua Forster on 5/4/2015.

public class FoodArrayAdapter extends ArrayAdapter<SingleMenuItem> {

    private ArrayList<SingleMenuItem> data;

    public FoodArrayAdapter(Context context, int resource,  ArrayList<SingleMenuItem> data) {
        super(context, resource, data);
        this.data = data;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        // check if view is null and inflate if it is
        if (convertView == null) {
            LayoutInflater layoutInflater = (LayoutInflater) parent.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.food_list_item, null);
        }

        SingleMenuItem food = data.get(position);

        if (food != null) {

            TextView nameTextView = (TextView) convertView.findViewById(R.id.name_tv);
            TextView priceTextView = (TextView) convertView.findViewById(R.id.price_tv);
            TextView descriptionTextView = (TextView) convertView.findViewById(R.id.description_tv);

            nameTextView.setText(food.getName());
            double price = food.getPrice();
            priceTextView.setText(Double.toString(price));
            descriptionTextView.setText(food.getDescription());
        }
        // return converted view
        return convertView;
    }
}
