package com.example.jfors_000.barhop;

//Created by Joshua Forster on 4/20/2015.

 public class Special {

    // KEYS for members
    public static final String SPECIAL_ID_KEY = "special_id";
    public static final String BAR_ID_KEY = "bar_id";
    public static final String DAY_KEY = "day";
    public static final String TIME_KEY = "time";
    public static final String PRICE_KEY = "price";
    public static final String NAME_KEY = "name";

    // member variables
    private int specialID;
    private int barID;
    private String day;
    private String time;
    private String price;
    private String name;

    public Special(String time, String price, String name) {
        this.time = time;
        this.price = price;
        this.name = name;
    }

    public Special(int specialID, int barID, String day, String time, String price, String description) {
        this.specialID = specialID;
        this.barID = barID;
        this.day = day;
        this.time = time;
        this.price = price;
        this.name = description;
    }

    public int getSpecialID() {
        return specialID;
    }

    public void setSpecialID(int specialID) {
        this.specialID = specialID;
    }

    public int getBarID() {
        return barID;
    }

    public void setBarID(int barID) {
        this.barID = barID;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
