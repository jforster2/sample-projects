package com.example.jfors_000.barhop;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import java.util.ArrayList;

public class BarsListActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bars_list);

        // Set a toolbar to replace the action bar.
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Log.i("ON CREATE", "herro");

        /*bars.add(new Bar("Bar 1", 1, "5 miles"));
        bars.add(new Bar("Bar 2", 5, "3 miles"));*/

        Log.i("REQUEST", "before newing");

        ObtainBarsData request = new ObtainBarsData(BarsListActivity.this);

        Log.i("REQUEST", "after newing");

        request.execute("Tennessee", "Clarksville");

        Log.i("EXECUTE", "after execute");
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_bars_list, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void setBars(final ArrayList<Bar> barArrayList) {
        ListView list = (ListView) findViewById(R.id.barsLV);
        BarArrayAdapter barArrayAdapter = new BarArrayAdapter(this, R.layout.bar_list_item, barArrayList);
        Log.i("BARS", barArrayList.toString());
        list.setAdapter(barArrayAdapter);   // display data in list

        list.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(getApplicationContext(), BarInfoActivity.class);
               // Intent intent = new Intent(getApplicationContext(), MappingActivity.class);
                intent.putExtra(Bar.BAR_ID_KEY, barArrayList.get(position).getBarID());
                intent.putExtra(Bar.NAME_KEY, barArrayList.get(position).getName());
                startActivity(intent);
            }
        });
    }
}
