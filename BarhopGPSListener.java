package com.almstech.barhop;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;

import android.content.Context;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;

public class BarhopGPSListener implements LocationListener {

	private LocationManager locationManager;
	private String provider;
	
	double currentLatitude = Integer.MAX_VALUE;
	double currentLongitude = Integer.MAX_VALUE;
	
	GoogleMap map;
	Button confirmClaimButton;
	
	Context context;

	//ClaimActivity.FilterClaimsManager filterClaims;
	
	public BarhopGPSListener( Context context, GoogleMap map ){
		
		this.context = context;
		
		this.map = map;

		 // Get the location manager
	    locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
	    
	    // Define the criteria how to select the locatioin provider -> use
	    // default
	    Criteria criteria = new Criteria();
	    
	    provider = locationManager.getBestProvider(criteria, false);
	    
	    Location location = locationManager.getLastKnownLocation(provider);
	    
	    if( location != null){
	    	
	    	currentLatitude = location.getLatitude( );
	    	currentLongitude = location.getLongitude( );
	    	
	    	CameraUpdate center=
			        CameraUpdateFactory.newLatLngZoom(new LatLng(location.getLatitude( ),
			        		location.getLongitude( )), 7);
			
			
			// Zoom in, animating the camera.
			map.animateCamera(CameraUpdateFactory.zoomTo(7), 2000, null);
			
			map.moveCamera( center );
			
			confirmClaimButton.setClickable( true );

	    }
	    
	    locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1000, 0, this);
	}
	
	@Override
	public void onLocationChanged(Location location) {
		// TODO Auto-generated method stub

		if( currentLatitude == Integer.MAX_VALUE && currentLongitude == Integer.MAX_VALUE){
			
			CameraUpdate center=
			        CameraUpdateFactory.newLatLngZoom(new LatLng(location.getLatitude( ),
			        		location.getLongitude( )), 15);
			
			
			map.moveCamera( center );
			// Zoom in, animating the camera.
			map.animateCamera(CameraUpdateFactory.zoomTo(14), 2000, null);
			
			
			confirmClaimButton.setClickable( true );
		}
		
		currentLatitude = location.getLatitude( );
		currentLongitude = location.getLongitude( );
 
		//Log.i("location changed", "onchange");
	}
	@Override
	public void onProviderDisabled(String provider) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void onProviderEnabled(String provider) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {
		// TODO Auto-generated method stub
		
	}
	
	public double getCurrentLatitude() {
		
		return currentLatitude;
	}

	public void setCurrentLatitude(double currentLatitude) {
		
		this.currentLatitude = currentLatitude;
	}

	public double getCurrentLongitude() {
		
		return currentLongitude;
	}

	public void setCurrentLongitude(double currentLongitude) {
	
		this.currentLongitude = currentLongitude;
	}
}

