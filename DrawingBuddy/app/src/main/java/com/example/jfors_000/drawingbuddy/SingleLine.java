package com.example.jfors_000.drawingbuddy;

import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;

/**
 * Created by JFORS_000 on 3/24/2015.
 *
 *  represents a single line object
 *      |> MyCanvasView will have an arrayList of SingleLine objects
 */
public class SingleLine {

    private Path line;              // the line
    private Paint lineAppearance;   // how the line looks
    private boolean eraser;         // true if this line is a ine drawn while the user is erasing instead of drawing

    public SingleLine() {
        this.lineAppearance = new Paint();
        this.line = new Path();
        lineAppearance.setStrokeWidth(10f);
        lineAppearance.setColor(Color.BLACK);
        lineAppearance.setStyle(Paint.Style.STROKE);
        lineAppearance.setStrokeJoin(Paint.Join.ROUND);
        eraser = false;
    }

    public SingleLine(Paint lineAppearance, Path line) {
        this.lineAppearance = lineAppearance;
        this.line = line;
        eraser = false;
    }

    public SingleLine(float brushWidth, int brushColor, boolean erasing) {
        this.lineAppearance = new Paint();
        this.line = new Path();
        lineAppearance.setStrokeWidth(brushWidth);
        lineAppearance.setColor(brushColor);
        lineAppearance.setStyle(Paint.Style.STROKE);
        lineAppearance.setStrokeJoin(Paint.Join.ROUND);
        eraser = erasing;
    }


    public Path getLine() {
        return line;
    }

    public void setLine(Path line) {
        this.line = line;
    }

    public Paint getLineAppearance() {
        return lineAppearance;
    }

    public void setLineAppearance(Paint lineAppearance) {
        this.lineAppearance = lineAppearance;
    }

    public boolean isEraser() {
        return eraser;
    }

    public void setEraser(boolean eraser) {
        this.eraser = eraser;
    }
}
