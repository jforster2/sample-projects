package com.example.jfors_000.drawingbuddy;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Toast;

import java.util.ArrayList;

/**
 * Created by JFORS_000 on 3/16/2015.
 */
public class MyCanvasView extends View {

    private SingleLine singleLine;
    private ArrayList <SingleLine> lines;
    private int currentBrushColor;
    private int currentBackgroundColor;
    private int currentWidth;
    private int currentHeight;
    private int currentLineNumber;
    private float currentBrushWidth;
    private Integer historySize;
    public boolean isErasingLine;

    public MyCanvasView(Context context) {
        this(context, null, 0);
    }

    public MyCanvasView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public MyCanvasView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        lines = new ArrayList<>();
        historySize = null;
        currentBackgroundColor = getResources().getColor(R.color.white);
        this.setBackgroundColor(currentBackgroundColor);
        currentBrushColor = Color.BLACK;
        currentBrushWidth = 10f;
        currentLineNumber = 0;
        isErasingLine = false;
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        currentWidth = w;
        currentHeight = h;
        super.onSizeChanged(w, h, oldw, oldh);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        //super.onDraw(canvas);

        //canvas.drawPaint(linePaint);
        //canvas.drawPath(path, linePaint);
        if (lines != null) {
            for (SingleLine s : lines) {
                if (s.isEraser()) {
                    s.getLineAppearance().setColor(currentBackgroundColor);
                }
                canvas.drawPath(s.getLine(), s.getLineAppearance());
            }
        }
    }

    // Set color of line drawn
    public void setBrushColor(int color) {
        currentBrushColor = color;
    }

    // get current brush color
    public int getCurrentBrushColor() {
        return currentBrushColor;
    }

    // Set brush width
    public void setLineWidth(float width) {
        currentBrushWidth = width;
    }

    // Set background color
    public void setCurrentBackgroundColor(int color) {
        currentBackgroundColor = color;
        this.setBackgroundColor(currentBackgroundColor);
    }

    // get current background color
    public int getCurrentBackgroundColor() {
        return currentBackgroundColor;
    }

    // Removes last line drawn if one exists
    public void undoLastAction() {
        if (!lines.isEmpty()) {
            lines.remove(lines.size() - 1);
            invalidate();   // Schedules a repaint
        } else {
            Toast.makeText(this.getContext(), "There is nothinng left to undo", Toast.LENGTH_SHORT).show();
        }
    }

    // new canvas
    public void newCanvas() {
        lines.clear();
        setCurrentBackgroundColor(Color.WHITE);
        invalidate();
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        float touchX = event.getX();
        float touchY = event.getY();

        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:

                // check if line is being drawn or user is drawing "eraser" line
                if (isErasingLine) {
                    singleLine = new SingleLine(currentBrushWidth, currentBackgroundColor, true);
                } else {
                    singleLine = new SingleLine(currentBrushWidth, currentBrushColor, false);
                }
                singleLine.getLine().moveTo(touchX, touchY);
                lines.add(singleLine);
                currentLineNumber = lines.indexOf(singleLine);
                //Log.i("ONTOUCHEVENT", "DOWN");
                return true;
            case MotionEvent.ACTION_MOVE:
                singleLine.getLine().lineTo(touchX, touchY);
                lines.set(currentLineNumber, singleLine);
                historySize = event.getHistorySize();
                //Log.i("ONTOUCHEVENT", "MOVE");
                break;
            case MotionEvent.ACTION_UP:
                if (historySize == null) {
                    singleLine.getLineAppearance().setStyle(Paint.Style.FILL);
                    singleLine.getLine().addCircle(touchX, touchY, 7.5f, Path.Direction.CW);
                    lines.set(currentLineNumber, singleLine);
                    Log.i("ONTOUCHEVENT", "DRAWING DOT");
                }
                historySize = null;

                Log.i("ONTOUCHEVENT", "UP");
                // FIGURE OUT WHY IF STATEMENT DOES NOT ALWAYS DRAW A DOT WHEN IT SHOULD
                //path.addCircle(touchX, touchY, 7.5f, Path.Direction.CW);
                break;
            default:
                return false;
        }

        // Schedules a repaint
        invalidate();
        return true;
    }
}
