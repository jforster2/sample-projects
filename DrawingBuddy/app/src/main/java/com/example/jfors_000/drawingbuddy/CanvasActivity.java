package com.example.jfors_000.drawingbuddy;

import android.graphics.Color;
import android.provider.MediaStore;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import java.util.UUID;

/**
 * FEATURE 1:   set background color
 *      -- CanvasActivity.java, MyCanvasView.java, menu_canvas.xml
 *
 * FEATURE 2:   eraser
 *      -- CanvasActivity.java, MyCanvasView.java, menu_canvas.xml, SingleLine.java
 *
 * FEATURE 3:   undo last action
 *      -- CanvasActivity.java, MyCanvasView.java, menu_canvas.xml
 *
 * FEATURE 4:   save current canvas to phone's photo gallery
 *      -- CanvasActivity.java, menu_canvas.xml, manifest.xml
 */

public class CanvasActivity extends ActionBarActivity {

    private final float widthThin = 5f;
    private final float widthMedium = 10f;
    private final float widthThick = 15f;
    private final float widthSuperSize = 30f;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_canvas);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_canvas, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        MyCanvasView myCanvasView = (MyCanvasView) findViewById(R.id.canvas_area);

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        } else if (id == R.id.action_background) {
            showColorWheel("background", myCanvasView.getCurrentBackgroundColor());
        } else if (id == R.id.action_line) {
            showColorWheel("line", myCanvasView.getCurrentBrushColor());
        } else if (id == R.id.line_thin) {
            myCanvasView.setLineWidth(widthThin);
        } else if (id == R.id.line_medium) {
            myCanvasView.setLineWidth(widthMedium);
        } else if (id == R.id.line_thick) {
            myCanvasView.setLineWidth(widthThick);
        } else if (id == R.id.line_super_size) {
            myCanvasView.setLineWidth(widthSuperSize);
        } else if (id == R.id.action_erase) {
            myCanvasView.isErasingLine = true;
        } else if (id == R.id.action_new_canvas) {
            myCanvasView.newCanvas();
        } else if (id == R.id.action_undo) {
            myCanvasView.undoLastAction();
        } else if (id == R.id.action_save) {
            myCanvasView.setDrawingCacheEnabled(true);
            String saved_image = MediaStore.Images.Media.insertImage(getContentResolver(), myCanvasView.getDrawingCache(), UUID.randomUUID().toString()+".png", "drawing");

            if (saved_image!=null) {
                Toast.makeText(this.getApplicationContext(), "Saved to Gallery", Toast.LENGTH_SHORT).show();
            } else  {
                Toast.makeText(this.getApplicationContext(), "Error", Toast.LENGTH_SHORT).show();
            }

            /** only needs to be called if buildDrawingCache is called manually */
            //myCanvasView.destroyDrawingCache();
        }

        return super.onOptionsItemSelected(item);

    }

    /** show dialog for color wheel */
    public void showColorWheel(final String selection, int currentColor) {
        final ColorWheelDialog colorWheelDialog = new ColorWheelDialog(this, currentColor, new ColorWheelDialog.OnColorSelectedListener() {

            MyCanvasView myCanvasView = (MyCanvasView) findViewById(R.id.canvas_area);

            @Override
            public void onColorSelected(int color) {
                if (selection == "line") {
                    myCanvasView.setBrushColor(color);
                    myCanvasView.isErasingLine = false;
                } else if (selection == "background") {
                    myCanvasView.setCurrentBackgroundColor(color);
                }
            }
        });
        colorWheelDialog.show();
    }
}
