package com.example.jfors_000.drawingbuddy;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;

/**
 * Created by JFORS_000 on 3/26/2015.
 */
public class ColorWheelDialog extends AlertDialog {

    private ColorWheel colorWheelView;
    private final OnColorSelectedListener onColorSelectedListener;

    /** constructor for alert dialog */
    public ColorWheelDialog(Context context, int initialColor, OnColorSelectedListener onColorSelectedListener) {
        super(context);

        this.onColorSelectedListener = onColorSelectedListener;

        /** set up the layout for the alert dialog */
        RelativeLayout colorWheelLayout = new RelativeLayout(context);
        LayoutParams layoutParams = new LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        layoutParams.addRule(RelativeLayout.CENTER_IN_PARENT);

        /** create the custom view for the color wheel itself */
        colorWheelView = new ColorWheel(context);
        colorWheelView.setColor(initialColor);

        colorWheelLayout.addView(colorWheelView, layoutParams);

        setButton(BUTTON_POSITIVE, context.getString(android.R.string.ok), onClickListener);
        setButton(BUTTON_NEGATIVE, context.getString(android.R.string.cancel), onClickListener);

        /** adds the color wheel to the alert dialog laayout */
        setView(colorWheelLayout);
    }

    /** onClick listener for alert dialog buttons */
    private OnClickListener onClickListener = new OnClickListener() {
        public void onClick(DialogInterface dialog, int confirmColor) {
            switch (confirmColor) {
                case BUTTON_POSITIVE:
                    int selectedColor = colorWheelView.getColor();
                    onColorSelectedListener.onColorSelected(selectedColor);
                    break;
                case BUTTON_NEGATIVE:
                    dialog.dismiss();
                    break;
            }
        }
    };

    public interface OnColorSelectedListener {
        public void onColorSelected(int color);
    }
}
